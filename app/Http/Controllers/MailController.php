<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Input;

class MailController extends Controller
{
	public function send(Request $request)
	{
		$this->validate($request, [
      'name'     =>  'required',
      'phone'  =>  'required',
      'subject'  =>  'required',
      'message' =>  'required'
    ]);
		$data = array(
      'name'      =>  $request->name,
      'phone'     =>  $request->phone,
      'subject'   =>  $request->subject,
      'message'   =>   $request->message
		);

		Mail::to('mail@acsentosa.com')->send(new SendMail($data));
		return back()->with('success', 'Youre message has been sent. Thanks for contacting us!');
	}
}
