<header class="header-style2 pink">
  <div class="top-nav top-nav-style2">
    <div class="azir-container">
      <div class="row align-items-center" style="height: 40px;">
        <div class="col-12 col-lg-6">
          <div class="top-nav_left">
            <div class="top-nav_left--content email inline-block"><i class="fas fa-envelope"></i>mail@acsentosa.com</div>
          </div>
        </div>
        <div class="col-12 col-lg-6">
          <div class="top-nav_right" style="font-size: 8pt;">
            through truth and transparency, sincere trust is established
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="header">
    <div class="header-middle">
      <div class="azir-container">
        <div class="row align-items-center">
          <div class="col-11 col-lg-3"><a class="logo" href="{{url('/')}}"><img src="{{ URL::asset('images/mainlogo.png') }}" style="height: 40px;" alt="logo"></a></div>
          <div class="col-1 col-lg-9">
            <div class="website-info">
              <div class="info-block">
                <div class="info-icon"><i class="fas fa-phone-alt"></i></div>
                <div class="info-content">
                  <p class="light-title">Call us:</p>
                  <h6 class="main-title">+62 21 720 9958</h6>
                </div>
              </div>
              <div class="info-block">
                <div class="info-icon"><i class="fas fa-map-marker-alt"></i></div>
                <div class="info-content">
                  <p class="light-title">Office Address:</p>
                  <h6 class="main-title">Graha Iskandarsyah 7th Floor</h6>
                </div>
              </div>
              <div class="info-block">
                <div class="info-icon"><i class="far fa-clock"></i></div>
                <div class="info-content">
                  <p class="light-title">Open Time:</p>
                  <h6 class="main-title">Mon-Fri: 09:00 - 17:00</h6>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-bottom" style="padding: 13px 0;">
      <div class="azir-container">
        <div class="row align-items-center justify-content-end">
          <div class="col-1 col-lg-8">
            <div class="navigation">
              <ul>
                <li class="navigtion-item"><a class="navigation-link" href="{{url('/about')}}">About Us</a><i class="submenu-opener"></i>
                </li>
                <li class="navigtion-item"><a class="navigation-link" href="{{url('/coals')}}">Trading</a><i class="submenu-opener"></i>
                </li>
                <li class="navigtion-item"><a class="navigation-link" href="{{url('/contact')}}">Contact</a></li>
              </ul>              
            </div>
          </div>
          <div class="col-11 col-lg-4">
            <div class="page-function_block d-flex align-items-center justify-content-end">
              <div class="page-function">
                <div class="function-btn inline-block"><a href="#" id="search"><i class="icon_search"></i></a></div>
                <div class="function-btn inline-block"><a href="#" id="open-menu-sidebar"><i class="icon_menu"></i></a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>