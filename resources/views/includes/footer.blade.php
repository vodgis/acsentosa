<footer class="pink">
  <div class="footer-top">
    <div class="azir-container">
      <div class="row justify-content-between">
        <div class="col-12 col-lg-4">
          <div class="logo"><img src="{{ URL::asset('images/logo-pink-2.png') }}" alt="logo" style="width: 50%;"></div>
          <p class="website-introduce"><q style="font-style:italic;">“The success and our ability to extend the life of the asset is closely linked to the effective application of
													forward planning, performance analysis, cost reduction and the effectiveness application of capital.”</q></p>
        </div>
        <div class="col-12 col-sm-6 col-md-5 col-lg-3">
          <h5 class="footer-title">Quick link</h5>
          <ul class="footer-links_group inline-block">
            <li><a class="footer-link" href="{{url('/about')}}">About</a></li>
            <li><a class="footer-link" href="{{url('/coals')}}">Trading</a></li>
            <li><a class="footer-link" href="{{url('/contact')}}">Contacts</a></li>
          </ul>
        </div>
        <div class="col-12 col-sm-6 col-md-7 col-lg-4">
          <h5 class="footer-title">Location</h5>
					<address class="website-introduce">
							Graha Iskandarsyah 7th Floor<br>
							Jl. Iskandarsyah Raya No. 66C<br>
							Kel. Melawai, Kec. Kebayoran Baru<br>
							Jakarta Selatan 12160, Prov. DKI Jakarta, Indonesia.
					</address>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-bottom">
    <div class="azir-container">
      <div class="row align-items-center">
        <div class="col-8">
          <p class="copyright">@2019 ACSentosa. All rights reserved by <a href="https://rumahscript.com">RumahScript</a>.</p>
        </div>
      </div>
    </div>
  </div>
</footer>
<script src="{{ URL::asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ URL::asset('js/slick.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.scrollUp.min.js') }}"></script>
<script src="{{ URL::asset('js/numscroller-1.0.js') }}"></script>
<script src="{{ URL::asset('js/chart.js') }}"></script>
<script src="{{ URL::asset('js/masonry.pkgd.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.countdown.min.js') }}"></script>
<script src="{{ URL::asset('js/main.js') }}"></script>