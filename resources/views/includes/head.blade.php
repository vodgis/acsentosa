<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
<title>ACSentosa</title>
<meta name="keywords" content="coal trading, coal Indonesia, coal mining, coal, Indonesia">
<meta name="author" content="rumahscript.com">
<link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/jquery-ui.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/custom_bootstrap.css') }}">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i&amp;display=swap">
<link rel="stylesheet" href="{{ URL::asset('css/fontawesome.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/elegant.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/slick.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/animate.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/icomoon.css') }}">
<link rel="shortcut icon" href="{{ URL::asset('images/fav.png') }}">
<script src="{{ URL::asset('js/jquery-3.4.0.min.js') }}"></script>