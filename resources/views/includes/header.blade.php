<header class="header-style3">
  <div class="top-nav top-nav-style2">
    <div class="azir-container">
      <div class="row align-items-center" style="height: 40px;">
        <div class="col-12 col-lg-6">
          <div class="top-nav_left">
            <div class="top-nav_left--content email inline-block"><i class="fas fa-envelope"></i>mail@acsentosa.com</div>
            <div class="top-nav_left--content phone inline-block" style="font-size: 10pt;"><i class="fas fa-phone-alt"></i> +62 21 720 9958</div>
          </div>
        </div>
        <div class="col-12 col-lg-6">
          <div class="top-nav_right" style="font-size: 8pt;">
            through truth and transparency, sincere trust is established
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="header">
    <div class="azir-container">
      <div class="row no-gutters align-items-center">
        <div class="col-6 order-1 col-lg-2 col-lg-2"><a class="logo" href="{{url('/')}}"><img src="{{ URL::asset('images/logowhite.png') }}" style="height: 50px;" alt="logo"></a></div>
        <div class="col-12 order-3 order-lg-2 col-1 col-lg-8">
          <div class="navigation text-right">
            <ul>
							<li class="navigtion-item"><a class="navigation-link" href="{{url('/about')}}">About Us</a><i class="submenu-opener"></i>
                
              </li>
              <li class="navigtion-item"><a class="navigation-link" href="{{url('/coals')}}">Trading</a><i class="submenu-opener"></i>
                
              </li>
              <li class="navigtion-item"><a class="navigation-link" href="{{url('/contact')}}">Contact</a></li>
            </ul>
          </div>
        </div>
        <div class="col-6 order-2 order-lg-3 col-lg-2">
          <div class="page-function text-right">
            <div class="function-btn inline-block"><a href="#" id="search"><i class="icon_search"></i></a></div>
            <div class="function-btn nav-bar inline-block"><a href="#" id="open-menu-sidebar"><i class="icon_menu"></i></a></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>