@extends('layouts.default')
@section('content')
@include('includes.header2')

<section class="contact-us">
  <div class="contact-infomation">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.1534295634283!2d106.80151604992982!3d-6.243501462851004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f16f2db74d11%3A0xd462b4dcc836c5cb!2sGraha%20Iskandarsyah%2C%20Melawai%2C%20Kec.%20Kby.%20Baru%2C%20Kota%20Jakarta%20Selatan%2C%20Daerah%20Khusus%20Ibukota%20Jakarta!5e0!3m2!1sen!2sid!4v1576953084335!5m2!1sen!2sid" width="100%" height="550" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    <div class="azir-container">
      <div class="infomation-box_wrapper">
        <div class="infomation-box">
          <h5 class="mb-20 h5">Infomation</h5>
          <ul class="ul-16">
            <li> <i class="fas fa-phone-alt"></i>+62 21 720 9958</li>
            <li> <i class="fas fa-map-marker-alt"></i>
            	<div style="display: inline-flex;line-height: 1.4em;">
            		Graha Iskandarsyah 7th Floor<br>
								Jl. Iskandarsyah Raya No. 66C<br>
								Kel. Melawai, Kec. Kebayoran Baru<br>
								Jakarta Selatan 12160 <br>
								Prov. DKI Jakarta, Indonesia.
							</div>
						</li>
            <li> <i class="fas fa-envelope"></i>mail@acsentosa.com</li>
          </ul>
          <h5 class="mb-20 h5">Opening Hours</h5>
          <ul class="opening-hours">
            <li>Monday - Friday<span>09:00 - 17:00</span></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="contact-content">
    <div class="azir-container">
      <div class="layout-2col-ti">
        <div class="row align-items-center">
          <div class="col-12 col-md-6">
            <div class="layout-2col-ti_left">
              <div class="contact-content_left">
                <div class="section-header-style-2">
                  <h1 class="h1">Let me help you</h1>
                  <p class="p-16">If you have any questions, please fill in the box below, We will reply to you as soon as possible</p>
								  @if ($message = Session::get('success'))
								   <div class="alert alert-success alert-block" style="padding: 20px 20px;background: #b7e6b7;margin-bottom: 20px;">
								    <button type="button" class="close btn-pink" style="padding: 0px 5px;float: right;margin-top: -5px;" data-dismiss="alert">×</button>
								    <strong>{{ $message }}</strong>
								   </div>
								  @endif
                  <form action="send" method="POST">
                  	{{ csrf_field() }}
                    <input class="input-form" autocomplete="off" required="" type="text" name="name" placeholder="Name">
                    <input class="input-form" autocomplete="off" required="" type="text" name="phone" placeholder="Phone">
                    <input class="input-form" required="" autocomplete="off" type="text" name="subject" placeholder="Subject">
                    <textarea class="textarea-form" required="" name="message" cols="30" rows="5" placeholder="Message"></textarea>
                    <button type="submit" class="btn-outline-pink">Send Message</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-6">
            <div class="layout-2col-ti_right">
              <div class="contact-content_right"><img class="img-fluid" src="{{ URL::asset('images/contactus.png') }}" alt="contact image"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
      $('.close').on('click', function(){
      	$(this).parent().hide();
      })
    });
</script>
@stop