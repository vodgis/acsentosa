@extends('layouts.default')
@section('content')
@include('includes.header2')

<section class="about">
  <div class="azir-container">
    <div class="page-header text-center">
      <h1 class="h1">About ACS</h1>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="about-content">
          <div class="about-part experience-part">
            <div class="row align-items-center">
              <div class="col-12 col-md-6">
                <div class="section-header-style-3">
                  <h6 class="h6" style="color: #828282;">"The success and our ability to extend the life of the asset is closely linked to the effective application of forward planning, performance analysis, cost reduction and the effectiveness application of capital"</h6>
                  <p class="p">Business activities of ACS at this time is centered at East Kalimantan Province. As for the territory covered such as Samarinda City, Balikpapan City and Kutai Kartanegara District. Our main focusesin carrying out the work are safety and cost effectiveness towards the fulfillment of the business requirements. To achieve those points, we have gone through a variety of proper risk management and with providing our labor a good and comfort working environment.</p>
                </div>
              </div>
              <div class="col-12 col-md-6">
                <img src="{{ URL::asset('images/about2.jpg') }}" alt="about us image" style="max-width: 100%;">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="about-content">
          <div class="about-part experience-part">
            <div class="row align-items-center">
              <div class="col-12 col-md-6">
                <img src="{{ URL::asset('images/about1.jpg') }}" alt="about us image" style="max-width: 100%;">
              </div>
              <div class="col-12 col-md-6">
                <div class="section-header-style-3">
                  <p class="p">The way we work is closely related to aggregate business units that are integrated as a whole so that all operations depend critically. In this environment, effectiveness of business planning, suitable design, compliance with every business process and sustainable focus on cost effectiviness are very important.</p>
                  <h6 class="h6" style="font-weight: bold;font-size: 17px;font-family: cerebrisans-regular;color: #828282;">"Through truth and transparency, sincere trust is established, which allow maximum tolerance for all to develop"</h6>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="call-to-action">
  <div class="azir-container">
    <div class="row justify-content-between align-items-center">
      <div class="col-12 col-lg-9">
        <div class="call-to-action_content text-center text-lg-left">
          <h3 class="h3 cta-title">Would you like to speak with us?</h3>
          <p style="font-size: 10pt;color: white;margin-bottom: 0;">we love to hear and happy to answer any questions.</p>
        </div>
      </div>
      <div class="col-12 col-lg-3 text-center" style="color: white;text-align: center;font-size: 14pt;font-weight: bold;"><i class="fas fa-phone-alt"></i> +62 21 720 9958</div>
    </div>
  </div>
</section>
<section class="about" style="background: #F6F5F0;margin-top: 0;padding: 150px 0 60px 0;">
  <div class="azir-container">
    <div class="about-content about-content_style2">
      <div class="about-part wwd-part">
        <div class="section-header-style-2 text-center">
          <h1 class="h1">Our Vision</h1>
          <p class="p">To become one of the best trading companies that focuses on effective and efficient management systems <br>that are effective for both the company and the social environment.</p>
        </div>
        <div class="row" style="line-height: 1.5em;color:#828282;">
          <div class="col-12" style="text-align:center;">
            <p class="p" style="font-size: 24px;">&bull;</p>
            <p class="p" style="margin-bottom: 30px">provide qualified mining products and services <br>that are consistent and also in accordance <br>with the wishes of the buyer</p>
            <p class="p" style="font-size: 24px;">&bull;</p>
            <p class="p" style="margin-bottom: 30px">responsible for business activities <br>that value the safety and environmental impact <br>of this business activities</p>
            <p class="p" style="font-size: 24px;">&bull;</p>
            <p class="p" style="margin-bottom: 30px">provide satisfactory sharing of business results <br>for all stakeholders</p>
            <p class="p" style="font-size: 24px;">&bull;</p>
            <p class="p">increase the productivity and income of the local community <br>which can make a positive contribution to the area <br>where these business activities take place</p>
          </div>
          <div class="col-12 col-md-6" style="text-align:left;">
          </div>
        </div>
        <div class="row" style="margin-top: 40px;line-height: 1.5em;color:#828282;">
          <div class="col-12 col-md-6" style="text-align:right;">
          </div>
          <div class="col-12 col-md-6" style="text-align:left;">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="about">
  <div class="azir-container">
    
  </div>
</section>
@stop