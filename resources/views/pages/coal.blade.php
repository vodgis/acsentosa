@extends('layouts.default')
@section('content')
@include('includes.header2')

<section class="about">
  <div class="azir-container">
    <div class="page-header text-center">
      <h1 class="h1">COAL MINING PRODUCTS OF PT. ACS</h1>
    </div>
		<div class="row">
			<div class="col-12">
			<div class="about-content">
				<h2 class="p text-center">EXECUTIVE SUMMARY OF COAL MINING PRODUCTS OF PT. ACS</h2>
				<p class="p">Here by we would like to attach a List of Coal Mining Products owned by PT. ACS to be offered to buyers. The list contains specifications and other information that would be needed by buyers.</p>
				<ol>
					<li><p class="p"><b>Spesification</b></p>
							<table width="100%" class="text-center">
								<tr>
									<td style="font-style:italic;"><p class="p">Mine Name</p></td>
									<td><p class="p">:</p></td>
									<td><p class="p">KSU Karya Desa & CV Jasa Andhika Raya</p></td>
								</tr>
								<tr>
									<td style="font-style:italic;"><p class="p">Location</p></td>
									<td><p class="p">:</p></td>
									<td><p class="p">East Kalimantan</p></td>
								</tr>
								<tr>
									<td style="font-style:italic;"><p class="p">Typical Spec.</p></td>
									<td><p class="p">:</p></td>
									<td><p class="p">CV: 5600 Kkal/kg GAR</p></td>
								</tr>
								<tr>
									<td style="font-style:italic;"><p class="p">TS</p></td>
									<td><p class="p">:</p></td>
									<td><p class="p">< 1% adb</p></td>
								</tr>
								<tr>
									<td style="font-style:italic;"><p class="p">Ash</p></td>
									<td><p class="p">:</p></td>
									<td><p class="p">< 10% adb</p></td>
								</tr>
								<tr>
									<td style="font-style:italic;"><p class="p">VM</p></td>
									<td><p class="p">:</p></td>
									<td><p class="p">Approx. 39% adb</p></td>
								</tr>
								<tr>
									<td style="font-style:italic;"><p class="p">HGI</p></td>
									<td><p class="p">:</p></td>
									<td><p class="p">40</p></td>
								</tr>
								<tr>
									<td style="font-style:italic;"><p class="p">AFT</p></td>
									<td><p class="p">:</p></td>
									<td><p class="p">1380o C</p></td>
								</tr>
								<tr>
									<td style="font-style:italic;"><p class="p">Hauling Distance</p></td>
									<td><p class="p">:</p></td>
									<td><p class="p">± 12 KM</p></td>
								</tr>
								<tr>
									<td style="font-style:italic;"><p class="p">Crusher Capacity</p></td>
									<td><p class="p">:</p></td>
									<td><p class="p">200-300 MT/Hour</p></td>
								</tr>
								<tr>
									<td style="font-style:italic;"><p class="p">Jetty Coordinate</p></td>
									<td><p class="p">:</p></td>
									<td><p class="p">508291E | 9962355N (MCT)</p></td>
								</tr>
								<tr>
									<td style="font-style:italic;"><p class="p">Dept of Water</p></td>
									<td><p class="p">:</p></td>
									<td><p class="p">8m</p></td>
								</tr>
								<tr>
									<td style="font-style:italic;"><p class="p">Anchorage Coordinate</p></td>
									<td><p class="p">:</p></td>
									<td><p class="p">0.26813395LS / 117.6208BT (Muara Berau Anchorage)</p></td>
								</tr>
								<tr>
									<td style="font-style:italic;"><p class="p">Jetty to Anchorage</p></td>
									<td><p class="p">:</p></td>
									<td><p class="p">82 Nmile</p></td>
								</tr>
								<tr>
									<td style="font-style:italic;"><p class="p">Price</p></td>
									<td><p class="p">:</p></td>
									<td><p class="p">US$ 49.91 FOB Barge</p></td>
								</tr>
							</table>
					</li>
				</ol>
				<p class="p">Thus the Company Profile and Executive Summary List we made for use as it should. Thank you for your attention and we hope that this cooperation can be carried out immediately and run as well as possible.</p>
			</div>
			</div>
			<!-- end col-12 -->
		</div>
		<br>
    <div class="row">
      <div class="col-12">
        <div class="about-content">
          <div class="about-part experience-part">
            <div class="row align-items-center">
              <div class="col-12 col-md-6">
                <div class="section-header-style-3">
                  <!--<h6 class="h6" style="color: #828282;">"The success and our ability to extend the life of the asset is closely linked to the effective application of forward planning, performance analysis, cost reduction and the effectiveness application of capital"</h6>-->
                  <p class="p">Business activities of ACS at this time is centered at East Kalimantan Province. As for the territory covered such as Samarinda City, Balikpapan City and Kutai Kartanegara District. Our main focusesin carrying out the work are safety and cost effectiveness towards the fulfillment of the business requirements. To achieve those points, we have gone through a variety of proper risk management and with providing our labor a good and comfort working environment.</p>
                </div>
              </div>
              <div class="col-12 col-md-6">
                <img src="{{ URL::asset('images/about2.jpg') }}" alt="about us image" style="max-width: 100%;">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="about-content">
          <div class="about-part experience-part">
            <div class="row align-items-center">
              <div class="col-12 col-md-6">
                <img src="{{ URL::asset('images/about1.jpg') }}" alt="about us image" style="max-width: 100%;">
              </div>
              <div class="col-12 col-md-6">
                <div class="section-header-style-3">
                  <p class="p">The way we work is closely related to aggregate business units that are integrated as a whole so that all operations depend critically. In this environment, effectiveness of business planning, suitable design, compliance with every business process and sustainable focus on cost effectiviness are very important.</p>
                  <h6 class="h6" style="font-weight: bold;font-size: 17px;font-family: cerebrisans-regular;color: #828282;">"Through truth and transparency, sincere trust is established, which allow maximum tolerance for all to develop"</h6>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="call-to-action">
  <div class="azir-container">
    <div class="row justify-content-between align-items-center">
      <div class="col-12 col-lg-9">
        <div class="call-to-action_content text-center text-lg-left">
          <h3 class="h3 cta-title">Would you like to speak with us?</h3>
          <p style="font-size: 10pt;color: white;margin-bottom: 0;">we love to hear and happy to answer any questions.</p>
        </div>
      </div>
      <div class="col-12 col-lg-3 text-center" style="color: white;text-align: center;font-size: 14pt;font-weight: bold;"><i class="fas fa-phone-alt"></i> +62 21 720 9958</div>
    </div>
  </div>
</section>
<section class="about">
  <div class="azir-container">
    
  </div>
</section>
@stop