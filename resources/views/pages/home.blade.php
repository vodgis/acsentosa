@extends('layouts.default')
@section('content')
@include('includes.header')
<section class="h3-banner">
  <div class="azir-container">
    <div class="row align-items-center justify-content-center">
      <div class="col-12">
        <div class="banner-content" style="color: white;line-height: 16pt;font-size:10pt;font-family: cerebrisans-regular;text-align: right;padding-top: 170px;">
          <p>The success and our ability to extend the life of the asset<br>is closely linked to the effective application of<br> forward planning, performance analysis, cost reduction <br>and the effectiveness application of capital.</p>
          <p style="margin-top: 10px;font-weight: bold;">Founder & CEO</p>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="h3-section1">
  <div class="azir-container">
    <div class="layout-2col-ti">
      <div class="row justify-content-between align-items-center" style="padding-top: 50px;">
        <div class="col-12 col-md-6">
          <div class="layout-2col-ti_left">
            <div class="benifit-illus" data-aos="fade-right"><img src="{{ URL::asset('images/homepage3/mainimg.png') }}" alt="illustration"></div>
          </div>
        </div>
        <div class="col-12 col-md-6 col-lg-5">
          <div class="layout-2col-ti_right"> 
            <div class="section-header-style-3 text-center text-md-left" data-aos="fade-left">
              <h1 class="h1">We are ..</h1>
              <p class="p" style="font-size: 12pt;">Limited liability corporation which has the business activities in trading of mining
													 products and established under the laws of Republic of Indonesia based on Deed of Incorporation
													 No. 06 dated on 8th February 2019 and made by Fajar Nurrachman Kamarulloh, S.H., M. Kn., Notary
													 at Bandung District. ACS itself has more specific business activities at coal mining product trade
													 (coal) which derive from several mining business permit area (Wilayah Izin Usaha
													 Pertambangan/WIUP) around East Kalimantan Province of Republic of Indonesia.
							</p><a class="btn-outline-pink" href="{{url('/about')}}">About us</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="call-to-action" style="margin-bottom: 120px;">
  <div class="azir-container">
    <div class="row justify-content-between align-items-center">
      <div class="col-12 col-lg-9">
        <div class="call-to-action_content text-center text-lg-left">
          <h3 class="h3 cta-title">Would you like to speak with us?</h3>
          <p style="font-size: 10pt;color: white;margin-bottom: 0;">we love to hear and happy to answer any questions.</p>
        </div>
      </div>
      <div class="col-12 col-lg-3 text-center" style="color: white;text-align: center;font-size: 14pt;font-weight: bold;"><i class="fas fa-phone-alt"></i> +62 21 720 9958</div>
    </div>
  </div>
</section>
<section class="h3-lastest-insight" data-aos="fade-in" data-aos-offset="-1300" data-aos-duration="1500" style="margin-bottom: 10px;">
  <div class="azir-container">
    <div class="row">
      <div class="col-12">
        <div class="section-header-style-1 text-center">
          <h1 class="h1" style="text-align: left;color: #1a88a0;margin-bottom: 30px;">Mine & Stockpile</h1>
        </div>
      </div>
      <div class="col-12">
        <div class="insights-group">
          <div class="insight-regular">
            <div class="insight-img"><img src="{{ URL::asset('images/homepage3/insight-1.jpg') }}" alt="insights image">
              <!--<p class="date">26 Nov 2019</p>-->
            </div>
            <!--<div class="insight-content"><a href="blog_detail.html">Marketing consultancy Digital Decisions hires Peter Hanford</a>
              <p class="author">by<span>Admin</span></p>
            </div>-->
          </div>
          <div class="insight-regular">
            <div class="insight-img"><img src="{{ URL::asset('images/homepage3/insight-2.jpg') }}" alt="insights image">
              <!--<p class="date">26 Nov 2019</p>-->
            </div>
            <!--<div class="insight-content"><a href="blog_detail.html">Marketing consultancy Digital Decisions hires Peter Hanford</a>
              <p class="author">by<span>Admin</span></p>
            </div>-->
          </div>
          <div class="insight-regular">
            <div class="insight-img"><img src="{{ URL::asset('images/homepage3/insight-3.jpg') }}" alt="insights image">
              <!--<p class="date">26 Nov 2019</p>-->
            </div>
            <!--<div class="insight-content"><a href="blog_detail.html">Marketing consultancy Digital Decisions hires Peter Hanford</a>
              <p class="author">by<span>Admin</span></p>
            </div>-->
          </div>
          <div class="insight-regular">
            <div class="insight-img"><img src="{{ URL::asset('images/homepage3/insight-1.jpg') }}" alt="insights image">
              <p class="date">26 Nov 2019</p>
            </div>
            <div class="insight-content"><a href="blog_detail.html">Marketing consultancy Digital Decisions hires Peter Hanford</a>
              <p class="author">by<span>Admin</span></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="h3-lastest-insight" data-aos="fade-in" data-aos-offset="-1300" data-aos-duration="1500">
  <div class="azir-container">
    <div class="row">
      <div class="col-12">
        <div class="section-header-style-1 text-center">
          <h1 class="h1" style="text-align: left;color: #1a88a0;margin-bottom: 40px;">Jetty Situation</h1>
        </div>
      </div>
      <div class="col-12">
        <div class="insights-group">
          <div class="insight-regular">
            <div class="insight-img"><img src="{{ URL::asset('images/homepage3/jetty-mct-1.jpg') }}" alt="insights image">
              <!--<p class="date">26 Nov 2019</p>-->
            </div>
            <!--<div class="insight-content"><a href="blog_detail.html">Marketing consultancy Digital Decisions hires Peter Hanford</a>
              <p class="author">by<span>Admin</span></p>
            </div>-->
          </div>
          <div class="insight-regular">
            <div class="insight-img"><img src="{{ URL::asset('images/homepage3/jetty-mct-2.jpg') }}" alt="insights image">
              <!--<p class="date">26 Nov 2019</p>-->
            </div>
            <!--<div class="insight-content"><a href="blog_detail.html">Marketing consultancy Digital Decisions hires Peter Hanford</a>
              <p class="author">by<span>Admin</span></p>
            </div>-->
          </div>
          <div class="insight-regular">
            <div class="insight-img"><img src="{{ URL::asset('images/homepage3/jetty-mct-3.jpg') }}" alt="insights image">
              <!--<p class="date">26 Nov 2019</p>-->
            </div>
            <!--<div class="insight-content"><a href="blog_detail.html">Marketing consultancy Digital Decisions hires Peter Hanford</a>
              <p class="author">by<span>Admin</span></p>
            </div>-->
          </div>
          <div class="insight-regular">
            <div class="insight-img"><img src="{{ URL::asset('images/homepage3/insight-1.jpg') }}" alt="insights image">
              <p class="date">26 Nov 2019</p>
            </div>
            <div class="insight-content"><a href="blog_detail.html">Marketing consultancy Digital Decisions hires Peter Hanford</a>
              <p class="author">by<span>Admin</span></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@stop